<html>
    <head>
        <title>Ajax Example</title>
        <script>
    function my_ajax_search(search_text,objId) {
     var xmlhttp=new XMLHttpRequest();   
     var server_page='server.php?search_text='+search_text;
     xmlhttp.open('GET', server_page);
     xmlhttp.onreadystatechange = function () {
         if(xmlhttp.readyState == 4 && xmlhttp.status == 200) {
             //alert(xmlhttp.responseText);
             document.getElementById(objId).innerHTML = xmlhttp.responseText;
         }
     }
     xmlhttp.send(null);
    }

</script>
    </head>
    <body onload="my_ajax_search('','res')">
        <h3>Ajax Search</h3>
        <span>Enter Name : </span>
        <input type="text" name="search_text" onkeyup="my_ajax_search(this.value,'res');">
        <hr/>
        <br/>
        <span id="res"></span>
    
    </body>
</html>
